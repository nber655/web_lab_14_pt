// Allow us to use the Express framework
var express = require('express');

// Specify that the app should use Formidable (to process file uploads)
var formidable = require("formidable");

// Specify that the app should use Jimp (to create image thumbnails)
var jimp = require("jimp");

// Specify that the app should use fs (to scan directory contents)
var fs = require("fs");

// Setup a new Express app
var app = express();

// The app should listen on port 3000, unless a different
// port is specified in the environment.
app.set('port', process.env.PORT || 3000);

// Specify that the app should use handlebars
var handlebars = require('express-handlebars');
app.engine('handlebars', handlebars({ defaultLayout: 'main' }));
app.set('view engine', 'handlebars');

// Reads all the images in the public/images/thumbnails folder, then
// renders the image-gallery/image-gallery view.
function renderImageGallery(req, res) {

    fs.readdir(__dirname + "/public/images/thumbnails/", function (err, files) { // Allow the app to scan the /public/images/thumbnails/ folder
        // files is an array of files (All files in the folder /public/images/thumbnails/)
        // The content of "files" is the name of the files located in the folder /public/images/thumbnails/
        var gallery = [];
        for (var i = 0; i < files.length; i++) { // If you get an error, then check that the /public/images/ folder contain the /thumbnails/ and /fullsize/ folders
            var file = files[i].toLowerCase(); // ignore case-sensitive
            if (file.endsWith(".png") || file.endsWith(".bmp") ||
                file.endsWith(".jpg") || file.endsWith(".jpeg")) {

                gallery[gallery.length] = file; // in the first execution, gallery.length is Zero (0) which correspond to the index of the array gallery
            }
        }

        var data = {
            pageTitle: "Image Gallery",
            gallery: gallery
        }

        res.render("image-gallery/image-gallery", data);

    });
}

// Specify that when we browse to "/" with a GET request, render the image gallery.
app.get('/', function (req, res) {
    renderImageGallery(req, res);
});

// Specify that when we browse to "/" with a POST request, process the file upload, then render the image gallery.
// Specify that when we submit the form (<form enctype="multipart/form-data" action="/" method="POST">) to "/" with a POST request / method, process the file upload, then render the image gallery.
app.post('/', function (req, res) {

    var form = new formidable.IncomingForm(); // Creates a new incoming form. // Incoming node request

    // Save uploaded files in the ./public/images/fullsize/ folder.
    // Event handler for the form's fileBegin event
    form.on("fileBegin", function (name, file) {
        // "name" will be the name of the <input> control on the form, while
        // "file" will contain info about the file itself. // <input type="file" id="fileUpload" name="fileUpload" accept=".jpg,.jpeg,.bmp,.png" required>

        // Setting the file's path here will change where the file will be uploaded to.
        file.path = __dirname + "/public/images/fullsize/" + file.name;
    });

    // This will begin parsing the form, and the given function will be called
    // once everything's complete.
    // then use the parse method to process the incoming node request.
    // To process the form submission
    form.parse(req, function (err, fields, files) {
        // The "fields" argument contains all the "normal" fields (e.g. name, email etc) // The input controls included in the form
        // The "files" argument contains information about any files which were uploaded, and
        // gives us the ability to access that file's data.
        // "fileUpload" here refers to the <input type="file" name="fileUpload"...> which we have
        // on our HTML form.

        // Get the file which was uploaded.
        var file = files.fileUpload;

        // Create a thumbnail for the file - max. size of 400x400px.
        // Read the uploaded image from disk
        jimp.read(__dirname + "/public/images/fullsize/" + file.name, function (err, image) { // Function to load an image
            // "image" argument represent the full-size image that is been loaded onto memory

            // When the image has been read, create its thumbnail by scaling it to fit inside a
            // 400x400 box, then save it.
            image
                .scaleToFit(400, 400)
                .write(__dirname + "/public/images/thumbnails/" + file.name, function (err) { // Save images in /public/images/thumbnails/ folder

                    // When the thumbnail has been saved, then render the image gallery.
                    // Add the image to "/".
                    renderImageGallery(req, res);

                });
        });

    });
});


// Allow the server to serve up files from the "public" folder. // to serve static files, such as images, CSS, JavaScript, etc.
app.use(express.static(__dirname + "/public"));

// Start the server running.
app.listen(app.get('port'), function () {
    console.log('Express started on http://localhost:' + app.get('port'));
});