// Allow us to use the Express frameworks
// (must be installed with npm install --save express).
var express = require('express');

// Setup a new Express app
var app = express();

// The app should listen on port 3000, unless a different
// port is specified in the environment.
app.set('port', process.env.PORT || 3000);

// Specify that the app should use handlebars
// (must be installed with npm install --save express-handlebars).
var handlebars = require('express-handlebars');
app.engine('handlebars', handlebars({ defaultLayout: 'main' }));
app.set('view engine', 'handlebars');

// Specify that the app should use the Formidable form processing library
// (must be installed with npm install --save formidable).
var formidable = require('formidable'); // allow us to use formidable library / framework

// Specify that when we browse to "/" with a GET request, show the example02/form view
app.get('/', function (req, res) {

    res.render("example02/form"); //use form.handlebars
});

// Specify that when we browse to "/" with a POST request, do a file upload.
// Specify that when we submit the form (<form enctype="multipart/form-data" method="POST" action="/">) to "/" with a POST request, do a file upload.
// check if the requesting url is ‘/’ and the method is ‘post’ 
app.post('/', function (req, res) {

    // Parse the form using Formidable
    var form = new formidable.IncomingForm(); // Creates a new incoming form. // Incoming node request

    // By default, Formidable will save uploaded files to some random temp directory somewhere.
    // This will change the File's upload location to somewhere we can control better - such as
    // an "uploads" folder within our project.
    // IMPORTANT: The folder you're uploading to MUST ALREADY EXIST or you will get an error!

    //Events: form.on("fileBegin" Emitted whenever a new file is detected in the upload stream. Use this event if
    // you want to stream the file to somewhere else while buffering the upload on
    // the file system.

    //Change the default location to where Formidable saves the temporary file.
    // This can be accomplished in the fileBegin event, emitted whenever a field / value pair has been received.

    form.on("fileBegin", function (name, file) {
        // "name" will be the name of the <input> control on the form, while
        // "file" will contain info about the file itself. // <input type="file" name="fileUpload"...>

        // Setting the file's path here will change where the file will be uploaded to.
        file.path = __dirname + "/uploads/" + file.name; // directory in the server where to store the file and file name
    });

    // This will begin parsing the form, and the given function will be called
    // once everything's complete.
     // then use the parse method to process the incoming node request.
    form.parse(req, function (err, fields, files) { // errors that ocure, normal fields, file uploaded
        // This function will be called once Formidable has finished parsing the form.

        // The "fields" argument contains all the "normal" fields (e.g. name, email etc) // The input controls included in the form
        // and can be used just like req.body in example01.
        console.log(fields.simpleName); // simpleName is the "name" of the input control that is included in the HTML code form.handlebars
        console.log(fields.simpleEmail);
        console.log(fields.simpleMyField);

        // The "files" argument contains information about any files which were uploaded, and
        // gives us the ability to access that file's data.
        // "fileUpload" here refers to the <input type="file" name="fileUpload"...> which we have
        // on our HTML form.
        var fileName = files.fileUpload.name; // The name of the file // fileUpload is the name of the input control in the HTML code form.handlebars
        var fileSize = files.fileUpload.size; // The size, in bytes
        var fileType = files.fileUpload.type; // The type of file, e.g. "image/png"
        console.log(files.fileUpload); // Uncomment this line and check the debug console to see many more potentially useful info you can get.

        var data = {
            name: fields.simpleName,
            email: fields.simpleEmail,
            fileName: fileName,
            fileSize: fileSize,
            fileType: fileType,
            // filePath: filePath
        };
        res.render("example02/submitForm", data);
    });
});

// Allow the server to serve up files from the "uploads" folder.
app.use(express.static(__dirname + "/uploads")); // Because of this line I can browse directly to the image from the web browser

/* We haven't specified any other routes, so browsing anywhere other than "/" will result in a default error page being returned. */

// Start the server running.
app.listen(app.get('port'), function () {
    console.log('Express started on http://localhost:' + app.get('port'));
});